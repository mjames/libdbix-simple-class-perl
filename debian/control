Source: libdbix-simple-class-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Mason James <mtj@kohaaloha.com>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libdbd-sqlite3-perl,
                     libdbix-simple-perl <!nocheck>,
                     libtest-simple-perl <!nocheck>,
                     libdbd-mysql-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libdbix-simple-class-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libdbix-simple-class-perl.git
Homepage: https://metacpan.org/release/DBIx-Simple-Class
Rules-Requires-Root: no

Package: libdbix-simple-class-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libdbix-simple-perl,
         libtest-simple-perl
Description: advanced object construction for DBIx::Simple
 DBIx::Simple::Class is a database table/row abstraction. At the same
 time it is not just a fancy representation of a table row like
 DBIx::Simple::Result::RowObject.
 .
 Using this module will make your code more organized, clean and reliable
 (separation of concerns + input-validation).
 You will even get some more performance over plain DBIx::Simple
 while keeping it's sexy features when you need them. Last but not
 least, this module has no other non-CORE dependencies besides DBIx::Simple
 and DBI.
